from rest_framework import routers
from .views import UserRegistrationViewSet,UserLoginViewset,LogoutView,SliderApi,ProductView,ProductReviewApi,OrderApi,CartApi,ConfirmedOrderApi
from django.urls import path

routers =routers.DefaultRouter()

urlpatterns=[
    path('registration',UserRegistrationViewSet.as_view(),name='apiregistration'),
    path('apilogin/',UserLoginViewset.as_view(),name='apilogin'),
    path('logout/',LogoutView.as_view(),name='logout'),
    path('sliders/',SliderApi.as_view(),name='slider'),
    path('product/',ProductView.as_view(),name='products'),
    path('review',ProductReviewApi.as_view(),name='review'),
    path('cart/',CartApi.as_view(),name='cart'),
    path('order/',OrderApi.as_view(),name='order'),
    path('confirm_order',ConfirmedOrderApi.as_view(),name='confirm_order')
]