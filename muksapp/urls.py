from django.urls import path,include
from .views import *

app_name = 'muksapp'

urlpatterns = [
    
    path('', index, name='index'),
    path('register/', Register.as_view(), name='register'),
    path('cart/', CartView.as_view(), name='cart'),
    path('product_detail/<str:product_slug>/', product_details, name='product_detail'),
    path('category/<str:category_slug>/', category_wise, name='category'),
    path('contactus/', ContactUs.as_view(), name='contactus'),
    path('remove_item/<int:product_id>/', remove, name='remove_item'),
    path('checkout/', Checkout.as_view(), name='checkout')
]
