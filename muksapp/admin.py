from django.contrib import admin
from .models import *

class ListingCategory(admin.ModelAdmin):
    list_display=('id','title','slug')
    list_filter=('title',)
    search_fields=('title','slug')
    list_per_page=20
admin.site.register(Category,ListingCategory)

class ListingImageCollection(admin.ModelAdmin):
    list_display= ('title','category','price','image','descriptions')
    list_filter=('title','category','price')
    search_fields=('title','category','price')
    list_per_page=20
admin.site.register(ImageCollection,ListingImageCollection)

class ListingCart(admin.ModelAdmin):
    list_display=('user','product')
    list_filter=('user','product')
    search_fields=('user','product')
    list_per_page=20
admin.site.register(Cart,ListingCart)

class ListingReview(admin.ModelAdmin):
    list_per_page=20
    list_display=('user','product','rating','comment','date','time')
    list_filter=('user','product','rating','date','time')
    search_fields=('user','product','rating','date','time')
admin.site.register(ProductReview,ListingReview)

class ListingSlider(admin.ModelAdmin):
    list_display= ('brand','title','quotes','image')
    list_filter=('brand','title')
    list_per_page=20
    search_fields=('brand','title')
admin.site.register(Slider,ListingSlider)

class ListingConfirmedOrder(admin.ModelAdmin):
    list_display=('user','totalprice')
    
    list_filter=('user','products')
    search_fields=('user','products')
    list_per_page=20
admin.site.register(ConfirmedOrder,ListingConfirmedOrder)

class ListingOrder(admin.ModelAdmin):
    list_display=('user','product')
    list_filter=('user','product')
    search_fields=('user','product')
    list_per_page=20
admin.site.register(Order,ListingOrder)
