from django.apps import AppConfig


class MuksappConfig(AppConfig):
    name = 'muksapp'
