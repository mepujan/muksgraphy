from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View
from .models import *
from .form import SignUpForm, ProductReviewForm, ContactUsForm, CheckoutForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# API 
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework import generics,status
from .serializers import *
from django.contrib.auth import logout


def index(request):
    category = Category.objects.all()
    slider = Slider.objects.all()
    recommendation1 = ImageCollection.objects.order_by('-id')[:4]
    recommendation2 = ImageCollection.objects.order_by('id')[:4]
    product = ImageCollection.objects.order_by('-id').all()
    page = request.GET.get('page')
    paginator = Paginator(product, 9)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    context = {
        'categories': category,
        'products': product,
        'posts': posts,
        'recommendations1': recommendation1,
        'sliders': slider,
        'recommendations2': recommendation2,
    }
    return render(request, 'index.html', context)


class BaseView(View):
    template_context = {
        'categories': Category.objects.order_by('title').all()
    }


class CartView(LoginRequiredMixin, BaseView):

    def get(self, request):
        self.template_context['cart_items'] = Cart.objects.filter(user=request.user).order_by('-id')
        total_amount = 0

        for cart_item in self.template_context['cart_items']:
            total_amount += cart_item.total()
        self.template_context['total_amount'] = total_amount
        grand_total = (total_amount + (13 / 100) * total_amount) - (0.1 * total_amount)
        self.template_context['grand_total'] = grand_total

        return render(request, 'cart.html', self.template_context)

    def post(self, request):
        item = Cart()
        order=Order()
        product = ImageCollection.objects.get(pk=request.POST.get('product_id'))
        item.product = product
        item.user = request.user
        item.save()
        order.product = product
        order.user=request.user
        order.save()
        return redirect('/cart')


class Register(View):
    def get(self, request):
        form = SignUpForm()
        return render(request, 'registration/register.html', {'form': form})

    def post(self, request):
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('registration/login')
        else:
            messages.error(request, 'Error in creating the User. Try Again... ')
        return render(request, 'registration/register.html', {'form': form})


def product_details(request, product_slug):
    product = ImageCollection.objects.get(slug=product_slug)
    similar1 = ImageCollection.objects.order_by('-id')[:4]
    similar2 = ImageCollection.objects.order_by('id')[:4]
    recommendation1 = ImageCollection.objects.order_by('-id')[:4]
    recommendation2 = ImageCollection.objects.order_by('id')[:4]

    categories = Category.objects.all()
    form = ProductReviewForm()

    if request.method == 'POST':
        form = ProductReviewForm(request.POST)

        if form.is_valid():
            review = form.save(commit=False)
            review.product = product
            review.user = request.user
            review.save()

    context = {
        'products': product,
        'categories': categories,
        'similar1': similar1,
        'similar2': similar2,
        'recommendations1': recommendation1,
        'recommendations2': recommendation2,
        'form': form,

    }

    return render(request, 'product-details.html', context)


def category_wise(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    categories = Category.objects.all()
    products = category.imagecollection_set.all()
    slider = Slider.objects.all()
    recommendation1 = ImageCollection.objects.order_by('-id')[:4]
    recommendation2 = ImageCollection.objects.order_by('id')[:4]
    page = request.GET.get('page')
    paginator = Paginator(products, 6)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    context = {
        'categories': categories,
        'category': category,
        'posts': posts,
        'recommendations1': recommendation1,
        'sliders': slider,
        'recommendations2': recommendation2,
    }

    return render(request, 'category_Products.html', context)


class ContactUs(View):
    def get(self, request):
        form = ContactUsForm()
        return render(request, 'contact-us.html', {'form': form})

    def post(self, request):
        form = ContactUsForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully submited message.")
        else:
            messages.error(request, "Error in submitting the message")
        return render(request, 'contact-us.html', {'form': form})


def remove(request, product_id):
    Cart.objects.filter(id=product_id).delete()
    return redirect('/cart')


class Checkout(View):
    def post(self, request):
        form = CheckoutForm(request.POST)
        cart = Cart.objects.filter(user=request.user)
        order = Order.objects.filter(user=request.user)
        if form.is_valid():
            confirm = form.save(commit=False)
            confirm.user = request.user
            confirm.save()
            for order_item in order:
                confirm.products.add(order_item)
            cart.delete()  
            return render(request, 'index.html')



# API View

class UserRegistrationViewSet(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserRegistrationSerializers


class UserLoginViewset(APIView):
    serializer_class = UserLoginSerializers
    permission_classes = ()

    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({'error': 'Wrong Credientials'}, status=status.HTTP_400_BAD_REQUEST)

class LogoutView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        logout(request)
        return Response(status=status.HTTP_200_OK)

class SliderApi(APIView):
    queryset=Slider.objects.all()
    serializer_class=SliderSerializers

    def get(self,request):
        slider=Slider.objects.all()
        serializer=SliderSerializers(slider,many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer=SliderSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductView(APIView):
    queryset=ImageCollection.objects.all()
    serializer_class=ProductSerializers

    def get(self,request):
        product=ImageCollection.objects.all()
        serializer=ProductSerializers(product,many=True)
        return Response(serializer.data)
    
    def post(self,request):
        product=ProductSerializers(data=request.data)
        
        if product.is_valid():
            product.save()
            return Response(product.data, status=status.HTTP_201_CREATED)
        return Response(product.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductReviewApi(APIView):
    queryset=ProductReview.objects.all()
    serializer_class=ProductReviewSerializers

    def get(self,request):
        review=ProductReview.objects.all()
        serializer=ProductReviewSerializers(review,many=True)
        return Response(serializer.data)

    def post(self,request):
        review=ProductReviewSerializers(data=request.data)
        if review.is_valid():
            review.save()
            return Response(review.data, status=status.HTTP_201_CREATED)
        return Response(review.errors, status=status.HTTP_400_BAD_REQUEST)

class CartApi(APIView):
    queryset=Cart.objects.all()
    serializer_class=CartSerializers

    def get(self,request):
        cart=Cart.objects.all()
        serializer=CartSerializers(cart,many=True)
        return Response(serializer.data)

    def post(self,request):
        cart=CartSerializers(data=request.data)
        if cart.is_valid():
            cart.save()
            return Response(cart.data, status=status.HTTP_201_CREATED)
        return Response(cart.errors, status=status.HTTP_400_BAD_REQUEST)

class OrderApi(APIView):
    queryset=Order.objects.all()
    serializer_class=OrderSerializers

    def get(self,request):
        order=Order.objects.all()
        serializer=OrderSerializers(order,many=True)
        return Response(serializer.data)

    def post(self,request):
        order=OrderSerializers(data=request.data)
        if order.is_valid():
            order.save()
            return Response(order.data, status=status.HTTP_201_CREATED)
        return Response(order.errors, status=status.HTTP_400_BAD_REQUEST)


class ConfirmedOrderApi(APIView):
    queryset=ConfirmedOrder.objects.all()
    serializer_class=ConfirmedOrderSerializers

    def get(self,request):
        confirm=ConfirmedOrder.objects.all()
        serializer=ConfirmedOrderSerializers(confirm,many=True)
        return Response(serializer.data)

    def post(self,request):
        confirm=ConfirmedOrderSerializers(data=request.data)
        if confirm.is_valid():
            confirm.save()
            return Response(confirm.data, status=status.HTTP_201_CREATED)
        return Response(confirm.errors, status=status.HTTP_400_BAD_REQUEST)

