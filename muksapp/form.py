from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import ProductReview, Feedback, ConfirmedOrder
from django.forms import ModelForm


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']


class ProductReviewForm(ModelForm):
    class Meta:
        model = ProductReview
        fields = ['comment', 'rating']


class ContactUsForm(ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'


class CheckoutForm(ModelForm):
    class Meta:
        model = ConfirmedOrder
        fields = ['totalprice']
