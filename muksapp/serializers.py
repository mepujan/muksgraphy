from rest_framework.authtoken.models import Token
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from .models import *

class UserRegistrationSerializers(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.is_active = True
        user.is_staff = True
        user.save()
        Token.objects.create(user=user)
        return User


class UserLoginSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']
        extra_kwargs = {'password': {'write_only': True}}


class SliderSerializers(serializers.ModelSerializer):
    class Meta:
        model=Slider
        fields='__all__'

class ProductSerializers(serializers.ModelSerializer):
    class Meta:
        model=ImageCollection
        fields='__all__'

class OrderSerializers(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields="__all__"

class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields="__all__"

class CartSerializers(serializers.ModelSerializer):
    class Meta:
        model=Cart
        fields='__all__'

class ConfirmedOrderSerializers(serializers.ModelSerializer):
    class Meta:
        model=ConfirmedOrder
        fields='__all__'

class ProductReviewSerializers(serializers.ModelSerializer):
    class Meta:
        model=ProductReview
        fields="__all__"