from datetime import datetime
from django.contrib.auth.models import User
from django.db import models
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField


class Slider(models.Model):
    brand = models.CharField(max_length=20, default='Muksgraphy')
    title = models.CharField(max_length=30)
    quotes = models.CharField(max_length=50)
    image = models.ImageField(upload_to='cms')

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = AutoSlugField(populate_from='title')

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class ImageCollection(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = AutoSlugField(populate_from='title')
    category = models.ForeignKey(Category, to_field='title', on_delete=models.CASCADE)
    price = models.FloatField()
    image = models.ImageField(upload_to='images')
    descriptions = RichTextField()

    def __str__(self):
        return self.title


class Feedback(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    feedback = models.CharField(max_length=200)

    def __str__(self):
        return self.email


class Cart(models.Model):
    user = models.ForeignKey(User, to_field='username', on_delete=models.CASCADE)
    product = models.ForeignKey(ImageCollection, to_field='title', on_delete=models.CASCADE)

    def total(self):
        return self.product.price

    def __str__(self):
        return self.product.title

class Order(models.Model):
    user = models.ForeignKey(User, to_field='username', on_delete=models.CASCADE)
    product = models.ForeignKey(ImageCollection, to_field='title', on_delete=models.CASCADE)

    def total(self):
        return self.product.price

    def __str__(self):
        return self.product.title


class ConfirmedOrder(models.Model):
    user = models.ForeignKey(User, to_field='username', on_delete=models.CASCADE)
    products = models.ManyToManyField(Order, related_name='cart')
    totalprice = models.FloatField()

    def __str__(self):
        return self.user.username


class ProductReview(models.Model):
    product = models.ForeignKey(ImageCollection, to_field='title', on_delete=models.CASCADE)
    user = models.ForeignKey(User, to_field='username', on_delete=models.DO_NOTHING)
    rating = models.IntegerField()
    comment = models.TextField()
    date = models.DateField(auto_now=True)
    time = models.TimeField(auto_now=True)

    def rating_range(self):
        return range(self.rating)

    def __str__(self):
        return self.user.username
